"""
Main dash file, here the dash app is created, dash server is started, and
the callbacksare created and linked with the objects
"""

################## Import Section ################
import dash
import numpy as np
from dash.exceptions import PreventUpdate
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State
import plotly.graph_objs as go
import os
from graphs import *
from html_objects.page_explore import EXPLORATORY
from html_objects.page_about import ABOUT
import json
import base64
import datetime
import io
import dash_table
import csv
from html_objects.page_simulation import SIMULATION
from html_objects.page_play import PLAY
from assets.styles import app_color
import pandas as pd
from data.departamentos_data import DepartamentosData
from data.prediction_data import predictLinear, getFeatures
from utils import upload_file
################ End Import Section ##############


################ Data sources creation ################
departments_data = DepartamentosData()
df_map = DepartamentosData().get_all_ScoreStats() 
df_gauges=departments_data.getstatsxgauges()
df_tten= departments_data.gettoptenScoresperInst()
df_barchar= DepartamentosData().getstatbarchar()
df_homicides = DepartamentosData().get_stat_homicides()
df_socialf= DepartamentosData().get_socialfactors()
############## End Data sources creation ##############

## Dash app server init
external_stylesheets = [
    {
        'href': 'https://fonts.googleapis.com/icon?family=Material+Icons',
        'rel': 'stylesheet'
    }
]
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
server = app.server


####################################### CALLBACKS SECTION #############################################


############# Page PLAY

## Boxplot for Reference Group Vs Score update, input: year, department
@app.callback(
        Output("boxplot_1", "figure"),
        [
            Input("year_slider_play", "value"),
            Input("stateselector", "value"),
            
        ]
)
def update_scatter_1(years,selected_dropdown_value):
    df_boxfilt=df_socialf
    df_boxfilt = df_boxfilt.loc[(df_boxfilt.year >= years[0]) & (df_boxfilt.year <= years[1]) & (df_boxfilt['dep_nombre'].isin(selected_dropdown_value)) ]
    return Boxplot_1.createboxplot_1(df_boxfilt)
    

## Scatterplot for SE Index Vs Score update, input: year, department
@app.callback(
        Output("scatter_1", "figure"),
        [
            Input("year_slider_play", "value"),
            Input("stateselector", "value"),
            
        ]
)
def update_scatter_1(years,selected_dropdown_value):
    df_scaterfilt=df_socialf
    df_scaterfilt = df_scaterfilt.loc[(df_scaterfilt.year >= years[0]) & (df_scaterfilt.year <= years[1]) & (df_scaterfilt['dep_nombre'].isin(selected_dropdown_value)) ]
    return Scatter_1.createscatter_1(df_scaterfilt)

    
## Bar chart for Nationality Vs Score update, input: year, department
@app.callback(
        Output("barchar_2", "figure"),
        [
            Input("year_slider_play", "value"),
            Input("countryselector", "value"),
            
        ]
)
def update_barchar_2 (years,selected_dropdown_value):
    df_barfil2=df_gauges
    df_barfil2 = df_barfil2.loc[(df_barfil2.year >= years[0]) & (df_barfil2.year <= years[1]) & (df_barfil2['estu_nacionalidad'].isin(selected_dropdown_value))]
    df_barfil2.prompuntglobal=df_barfil2.prompuntglobal.astype(float)
    df_barfil2 = df_barfil2.groupby(['estu_nacionalidad']).agg(
        {
            'prompuntglobal':'mean',
            'conteo':'sum'
        }
    ).reset_index().sort_values(by=["prompuntglobal"],ascending=False).head(20)
    df_barfil2.prompuntglobal = df_barfil2.prompuntglobal.round(1)
    return Barchar_2.createbarchar_2(df_barfil2)

## Bar chart for Homicide rates Vs Score update, input: year, department
@app.callback(
        Output("barchar_1", "figure"),
        [
            Input("year_slider_play", "value"),
            Input("stateselector", "value")
            
        ]
)
def update_barchar_1 (years,selected_dropdown_value):
    df_barfil = df_homicides
    df_barfil.year = df_barfil.year.astype(int)
    df_barfil.meanscore = df_barfil.meanscore.astype(float)
                   
    df_barfil = df_barfil.loc[(df_barfil.year >= years[0]) & (df_barfil.year <= years[1]) & (df_barfil['state'].isin(selected_dropdown_value)) ]
    df_barfil = df_barfil.groupby(['id_dep', 'state']).mean().reset_index().sort_values(by=['meanscore'],ascending=False)
    df_barfil.rate_homicides = df_barfil.rate_homicides.round(0)
    df_barfil.meanscore = df_barfil.meanscore.round(1)
    return Barchar_1.createbarchar_1(df_barfil)


## Gauge indicator update, input: year, country
@app.callback(
    Output("my-daq-leddisplay2", "value"),
    [
        Input("year_slider_play", "value"),
        Input("countryselector", "value")
    ],
)
def update_leddisplay2 (years,selected_dropdown_value):
    df_gaugesfil=df_gauges
    df_gaugesfil = df_gauges.loc[(df_gauges.year >= years[0])&(df_gauges.year <= years[1]) & (df_gauges['estu_nacionalidad'].isin(selected_dropdown_value))   ]
    
    valor = df_gaugesfil['conteo'].sum()
    return valor


## Alt Gauge indicator update, input: year, country
@app.callback(
    Output("my-daq-gauge2", "value"),
    [
        Input("year_slider_play", "value"),
        Input("countryselector", "value")
    ],
)
def update_gauge2 (years,selected_dropdown_value):
    df_gaugesfil= df_gauges
    df_gaugesfil = df_gauges.loc[(df_gauges.year >= years[0])&(df_gauges.year <= years[1]) & (df_gauges['estu_nacionalidad'].isin(selected_dropdown_value))   ]
    valor = df_gaugesfil['prompuntglobal'].mean()
    #print ("Valor de gaug:",valor)
    return valor

############# End Page PLAY

############# Page EXPLORE

## Colombia Map heatmap update, input: years
@app.callback(
    Output("choropleth", "figure"),
    [
        Input("year_slider", "value"),
    ],
)
def update_choro(year_range):
    df_map_n = df_map
    df_map_n.year = df_map_n.year.astype(int)
    df_map_n = df_map[(df_map.year >= year_range[0]) & (df_map.year <= year_range[1])]
    df_map_n = df_map_n.groupby(['id_dep', 'dep_nombre']).mean().reset_index()
    df_map_n.punt_global = df_map_n.punt_global.round(1)
    return colombia_map.generate_dep_choro(df_map_n)

## Line/bar plot of departments and Card/Indicators update, input: years, selected departments
@app.callback(
    [
        Output("aggregate_graph", "figure"),
        Output("info-container", "children"),
    ],
    [
        Input("choropleth", "selectedData"),
        Input("year_slider", "value"),

    ],
)
def make_aggregate_figure(selectedDep, year_slider):
    return aggregate_lineplot.lineplot(selectedDep, year_slider), cards.calc(selectedDep, year_slider)

## Table for Top 5 Universities update, input: year
@app.callback(
        Output("tabletop", "children"),
        [
            Input("year_slider", "value"),
        ]
)
def update_tabletopten (years):
    df_ttenfil=df_tten
    df_ttenfil = df_tten.loc[(df_tten.year >= years[0]) & (df_tten.year <= years[1])]   
    df_ttenfil.meanscore = df_ttenfil.meanscore.astype(float) 
    df_ttenfil = df_ttenfil.groupby(['university']).mean().sort_values(
        by=["meanscore"], ascending=False).reset_index()[:5]
    df_ttenfil['Year'] = "{}-{}".format(years[0],years[1])
    df_ttenfil.meanscore = df_ttenfil.meanscore.round(1)
    df_ttenfil.totalstudents = df_ttenfil.totalstudents.round(0)
    df_ttenfil.columns = ['University', 'Total students', 'Average score', 'Year']
    return dbc.Table.from_dataframe(df_ttenfil, bordered=True, dark=True, hover=True, responsive=True, striped=True)
   
############# End Page EXPLORE

############# Page Selector, input: url

@app.callback(
    Output("tabs-content", "children"),
    [Input("url", "pathname")]

    )
def render_content(pathname):
    if pathname == '/explore':
        return EXPLORATORY
    
    elif pathname == '/simulate':
        return SIMULATION
    
    elif pathname == '/play':
        return PLAY

    elif pathname == '/about':
        return ABOUT

############# End Page Selector

############# Page TRY IT

## Datatable for custom data uploaded by user, input: file selector
@app.callback([
    Output('output-data-upload', 'children'),
    Output('simu-button-space', 'children'),
    Output('simulation-data', 'data'),],
    [Input('upload-data', 'contents')],
    [State('upload-data', 'filename'),
    State('upload-data', 'last_modified')])
def update_output_file(list_of_contents, list_of_names, list_of_dates):
    if list_of_contents is not None:
        children, data = parse_csv.parse_contents(list_of_contents[0], list_of_names[0], list_of_dates[0])
        return children[0], children[1], data
    

## REAL TIME PREDICTION calling HTTP services, input: uploaded data
@app.callback(
    dash.dependencies.Output('simulation_result', 'children'),
    [dash.dependencies.Input('submit_simulation', 'n_clicks')],
    [dash.dependencies.State('simulation-data', 'data')])
def update_simulation_result(clicks, data):
    if clicks > 0:
        df = pd.DataFrame.from_dict(data, orient='index')
        df.to_csv('/tmp/test.csv', index=False, sep=';')
        _, filename = upload_file('/tmp/test.csv')
        status, df_result = predictLinear({'filename':filename.split('/')[-1]})
        if not status:
            return dbc.Alert("Something went wrong with the simulation", color="danger")
        df['score'] = df_result['score'].tolist()
        return simulation_result.result(df)


## REAL TIME VARIABLE ANALYSIS calling HTTP services, input: selected department
@app.callback(
    [dash.dependencies.Output('simulation_radar', 'figure'),
     dash.dependencies.Output('simulation_features_bar', 'figure')],
    [dash.dependencies.Input('simulation_department', 'value')])
def update_features_grapsh(department):
    status, df_result = getFeatures({'department':department})
    if not status:
        return dbc.Alert("Something went wrong with the simulation", color="danger"), dbc.Alert("Something went wrong with the simulation", color="danger")
    return features_graphs.features(df_result)

############# End Page TRY IT

##################################### END CALLBACKS SECTION ###########################################



#### Nav Bar creation and menus definition
app.title='EstudiApp'
PLOTLY_LOGO = "https://cdn4.iconfinder.com/data/icons/business-graphs-charts-set-1-1/256/Business_Graphs__Charts-22-512.png"

logo = dbc.Navbar(
    [
        html.A(
            dbc.Row(
                [
                    dbc.Col(html.Img(src=PLOTLY_LOGO, height="70px")),
                    dbc.Col(dbc.NavbarBrand([html.H1("EstudiApp", style={'font-size': "xxlarge", 'font-family':'DejaVu Sans Mono, monospace', 'font-weight': 'bold'}),html.P("Prepare yourself for the SaberPro Test")])
                    ),
                ],
                align="left",
                no_gutters=True,
            ),
            href="http://ds4a-2020-1183523121.us-east-1.elb.amazonaws.com/",
        ),
        dbc.Nav(
            [
                dbc.NavItem(dbc.NavLink("Explore", href="/explore")),
                dbc.NavItem(dbc.NavLink("Play", href="/play")),
                dbc.NavItem(dbc.NavLink("Try It", href="/simulate")),
                dbc.NavItem(dbc.NavLink("About Us", href="/about"))
            ],
            className="ml-auto mr-5", 
            navbar=True,
            fill=True,
            style={'font-size': "medium",}
        ),
    ],
    color="dark",
    dark=True,
    className="mb-5",
)


## Main Layout creation
app.layout = html.Div(
    [   
        dcc.Location(id="url", pathname="/explore"),
        logo,
        dbc.Container(id='tabs-content', fluid=True),
    ]
)
## Note that the layout for each page can be found in the html_objects folder
 

## Dash/Flask server start
@app.server.route("/ping") 
def ping(): 
    return "{status: ok}"

if __name__ == "__main__":
    debug = True
    if 'DEBUG' in os.environ:
        debug = False
    app.run_server(debug=debug, port=8050, host='0.0.0.0')
 