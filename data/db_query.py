"""
Data Access Object in order to centralize the way the app communicate with the database
"""

import psycopg2
import os
from cachetools import cached, TTLCache
from pandas import DataFrame

## Singleton pattern to minimize the database connections
class Singleton:

    def __init__(self, cls):
        self._cls = cls

    def Instance(self):
        try:
            return self._instance
        except AttributeError:
            self._instance = self._cls()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._cls)

## DAO class tha will be used for specific queries
@Singleton
class DAO:
    
    ## Constructor with the environment variables
    def __init__(self):
        self.connection = psycopg2.connect(user=os.environ['DB_USER'],
                                  password=os.environ['DB_PASS'],
                                  host=os.environ['DB_HOST'],
                                  port=os.environ['DB_PORT'],
                                  database=os.environ['DB_NAME'])
        self.cursor = self.connection.cursor()
        pass
    
    ## Query execution, returns array of dicts
    def executeQuery(self, query):
        try:
            self.cursor.execute(query)
            return self.cursor.fetchall()
        except (Exception, psycopg2.Error) as error :
            print ("Error while fetching data from PostgreSQL", error)
    
    ## Query execution, returns dataframe
    def executeQuery_df(self, query):
        try:
            df = DataFrame(self.executeQuery(query))
            df.columns=[ x.name for x in self.cursor.description]
            return df.copy()
        except (Exception, psycopg2.Error) as error :
            print ("Error while fetching data from PostgreSQL", error)

    ## Deletion of the session/object
    def __del__(self):
        if(self.connection):
            self.cursor.close()
            self.connection.close()
            print("PostgreSQL connection is closed")
