"""
Specific Database queries. Each function has a different query to an specific use inside the app
"""

from data.db_query import DAO
from cachetools import cached, TTLCache
import requests
import json
import pandas as pd

class DepartamentosData:

    def __init__(self):
        self.db = DAO.Instance()
    
    def getScoreStats(self, departaments, years=[1900,3000]):
        sep = "','"
        query = "select * from mv_getscorestats_v5 g  where g.dep_nombre IN ('{}') and g.year between '{}' and '{}';".format(sep.join(departaments), years[0], years[1])
        return self.db.executeQuery_df(query)

    def getNationalScore(self, years=[1900,3000]):
        query = "select g.year, avg(g.mean_punt_global) as mean_punt_global, avg(max_punt_global) as max_punt_global, avg(homdep_tasahomicidios) as homdep_tasahomicidios from mv_getscorestats_v5 g where g.year between '{}' and '{}' GROUP BY g.year order by g.year;".format(years[0], years[1])
        return self.db.executeQuery_df(query)

    def getDepartamentos(self):
        s="""select mp.estu_cod_depto_presentacion_norm ,mp.dep_nombre , avg(mp.avg ) as prom
                from mv_promedio_periodo_departament mp
                group by mp.estu_cod_depto_presentacion_norm ,mp.dep_nombre
                order by avg(mp.avg)
                """
        return self.db.executeQuery_df(s)

    
    def gettoptenScoresperInst(self, years=[1900,3000]):
        s="""
             select mgpv.university, mgpv.totalstudents,mgpv.meanscore, mgpv."year" 
                from mv_getscore_perinst_v4 mgpv 
                order by mgpv."year" desc, mgpv.meanscore desc
            
        """
        return self.db.executeQuery_df(s)


    def get_all_ScoreStats(self):
        query = """select estu_cod_reside_depto_norm as id_dep, dep_nombre, punt_global, year from mv_getscorestats_v2"""
        return self.db.executeQuery_df(query)    

    def getstatsxgauges(self,years=[1900,3000]):
        query="""select mg."year",mg.dep_nombre,mg.estu_nacionalidad , mg.prompuntglobal,mg.count as conteo
                    from mv_getstatsxgauges_v2 mg  """       
        return self.db.executeQuery_df(query)    

    def getcountries(self,years=[1900,3000]):
              
        query="""select mg.estu_nacionalidad 
                    from mv_getstatsxgauges_v2 mg
                    group by mg.estu_nacionalidad 
                    order by mg.estu_nacionalidad  
                    """
        return self.db.executeQuery_df(query)    



    def getstatbarchar(self):
        query = """select estu_cod_reside_depto_norm as id_dep, dep_nombre as state,
        punt_global as meanscore, year
        from mv_getscorestats_v2"""           
        return self.db.executeQuery_df(query)   

    def get_stat_homicides(self):
        query = """select estu_cod_reside_depto_norm as id_dep,
        dep_nombre as state,
        mean_punt_global as meanscore,
        year,
        homdep_tasahomicidios as rate_homicides
        from mv_getscorestats_v5"""           
        return self.db.executeQuery_df(query)  
    
    def get_socialfactors(self):
        query = """select sf."year" ,sf.dep_nombre,sf.estu_inse_individual,sf.estu_metodo_prgm,sf.gruporeferencia,sf.punt_global 
                    from mv_getsocialfactors_v2 sf TABLESAMPLE BERNOULLI(10);"""           
        return self.db.executeQuery_df(query)  
