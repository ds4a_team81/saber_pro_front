"""
HTTP request as functions. Calls the backend in order to call the lambda prediction engine
"""

import requests
import json
import pandas as pd

## Calls the Linear trained model, and returns a coded Pandas dataframe
def predictLinear(data):
    print('Linear requested')
    r = requests.post('https://tawh41m124.execute-api.us-east-1.amazonaws.com/predictLinear', data=data, headers={'Content-Type':'application/json'})
    print('Linear responded')
    if r.status_code == 200:
        df = pd.read_json(r.json()['data'], orient='index')
        return True, df
    else:
        return False, None

## Calls the XGBoost trained model, and returns a coded Pandas dataframe
def predictXgboost(data):
    r = requests.post('https://tawh41m124.execute-api.us-east-1.amazonaws.com/predictXgboost', data=data, headers={'Content-Type':'application/json'})
    if r.status_code == 200:
        df = pd.read_json(r.json()['data'], orient='index')
        return True, df
    else:
        return False, None

## Calls the Shapley analysis, and returns a coded Pandas dataframe
def getFeatures(data):
    print('Features requested')
    r = requests.post('https://tawh41m124.execute-api.us-east-1.amazonaws.com/getFeatures', data=data, headers={'Content-Type':'application/json'})
    print('Features responded')
    if r.status_code == 200:
        df = pd.read_json(r.json()['data'], orient='index')
        return True, df
    else:
        return False, None
