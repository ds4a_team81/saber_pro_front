import json
import pandas as pd
import plotly.graph_objs as go
from assets.styles import app_color, color_palette


def generate_dep_choro(df_map):
    #some columns are processed and transformed for use in the Choropleth function
    df_map.id_dep=df_map.id_dep.astype(str)
    df_map.loc[df_map.id_dep=='8','id_dep']='08'
    df_map.loc[df_map.id_dep=='5','id_dep']='05'

#the geojson is loaded with the information of the Colombia's map
    with open('assets/colombia.geo.json') as response:
        colGeo = json.load(response)


    state_data = [
    go.Choropleth(
            colorscale=color_palette,#map color
            hoverinfo='z+text',#data to be shown in the hover
            geojson=colGeo,#load the geojson in the Choropleth
            locations=df_map.id_dep, #The ID of each deparment
            featureidkey="properties.DPTO",#The ID of each deparment in the Geojson
            z=df_map.punt_global,#This define the color's map and is given by the mean score
            text=df_map.dep_nombre,#The column with the department's name
            colorbar_len=0.7, #The size of the colorbar
            colorbar_tickcolor='#FFFFFF', #The color of the tick in the color bar
            colorbar_tickfont={'family':"Open Sans, sans-serif",'color': '#FFFFFF', 'size':12}, #The color of the tick in the color bar
            colorbar_title={'text': 'Average <BR>score', 'font': {'family':"Open Sans, sans-serif",'color': '#FFFFFF', 'size':14}} #The title and its style of the color bar
        )
    ]
    layout = dict(      
        autosize=False,#False for manually define the size of the map
        clickmode="event+select",#active the mode click in the map
        paper_bgcolor='rgba(0,0,0,0)',
        margin=dict(l=0, r=0, t=0, b=0, pad=0),#the margin for the map
        #width=0,
        height=700,
        geo=go.layout.Geo(
            projection=go.layout.geo.Projection(type="mercator"),
            subunitcolor='rgba(0,0,0,0)',
            scope="south america",
            showcountries= False,
            bgcolor='rgba(0, 0, 0, 0)',
            fitbounds="locations"
        ),
    )
    return {"data": state_data, "layout": layout}