import pandas as pd
import plotly.graph_objs as go
from assets.styles import app_color, color_palette
import plotly.express as px
##Sactter plot of Socio-Economic Index  vs score
def createscatter_1(df):
    
    fig = px.scatter(df, 
                    x="estu_inse_individual", 
                    y="punt_global", 
                    color="dep_nombre",
                    labels={'punt_global':'Score','estu_inse_individual':'Social Economic Index','dep_nombre':'State'}, 
                    template='plotly_dark').update_layout(
                              {
                               'plot_bgcolor': 'rgba(0, 0, 0, 0)',
                               'paper_bgcolor': 'rgba(0, 0, 0, 0)',
                               'title_text':'Social Economic Index vs Score'
                                }
                                )                   
    return fig