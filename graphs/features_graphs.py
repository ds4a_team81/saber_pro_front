import dash_core_components as dcc
import dash_html_components as html
from assets.styles import line_colors
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
import pandas as pd

features_labels = {
    'ESTU_INSE_INDIVIDUAL':'SocioEconomic Index',
    'AGE_APROX':'Age', 
    'ESTU_METODO_PRGM_DISTANCIA/SEMI-PRESENCIAL':'Virtual Education',
    'ESTU_VALORMATRICULAUNIVERSIDAD_Entre 500 mil y menos de 4 millones': 'Fee between 500k y 4M COP',
    'INST_CARACTER_ACADEMICO_UNIVERSIDAD':'Studies in University',
    'INST_NOMBRE_INSTITUCION_UNIVERSIDAD NACIONAL DE COLOMBIA':'Studies in UNAL',
    'TASA_HOMICIDIOS_4_LOG':'High homicides rate',
    'ESTU_VALORMATRICULAUNIVERSIDAD_Menos de 500 mil':'Fee fewer than 500k',
    'GRUPOREFERENCIA_INGENIERIA':'Engineering career'
}

def features(data):

    target_quintile = 0
    data['label'] = data['feature'].apply(lambda x: features_labels[x])
    bars = go.Figure()
    bars.add_trace(
        go.Bar(
            x=data[data['quintile']==target_quintile]['label'],
            y=data[data['quintile']==target_quintile]['normalized_value'],
            marker=dict(color=line_colors),
        )
    )
    bars.update_layout(
        title='Lower Scores Features',
        xaxis=dict(
            titlefont_size=16,
            tickfont_size=14,
        ),
        yaxis=dict(
            title='Relevance',
            titlefont_size=16,
            tickfont_size=14,
        ),
        legend=dict(
            x=0,
            y=1.0,
            bgcolor='rgba(0,0,0,0)',
        ),
        font=dict(
            size=12,
            color="White"
        ),
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0, 0, 0, 0)',
        bargap=0.15, # gap between bars of adjacent location coordinates.
        bargroupgap=0.1 # gap between bars of the same location coordinate.
    )
    
    radar = go.Figure()

    for i in data['quintile'].unique():
        radar.add_trace(
            go.Scatterpolar(
                r=data[data['quintile'] == i]['normalized_value'],
                theta=data[data['quintile'] == i]['label'],
                fill='toself',
                name = 'Quintile {}'.format(i+1)
            )
        )

    radar.update_layout(
        polar=dict(
            radialaxis=dict(
                visible=False
            )
        ),
        showlegend=True,
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0, 0, 0, 0)',
        font=dict(
            size=12,
            color="White"
        ),
    )
    
    return radar, bars
