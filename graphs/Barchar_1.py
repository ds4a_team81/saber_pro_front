import pandas as pd
import plotly.graph_objs as go
from assets.styles import app_color, color_palette
import plotly.express as px


def createbarchar_1(df):
    ##make the first graph, it is a bar plot of deparmets vs mean score
    fig = px.bar(df, x='state', y='meanscore',
             hover_data=['state', 'meanscore'], 
             color='meanscore',
             color_continuous_scale=color_palette,
             labels={'state':'State','meanscore':'MeanScore'}, 
             height=500,
             template='plotly_dark').update_layout(
                              {
                               'plot_bgcolor': 'rgba(0, 0, 0, 0)',
                               'paper_bgcolor': 'rgba(0, 0, 0, 0)',
                               'title_text':'Mean score and homicide rates by State'
                                }
                                )
    ##make the second graph, it is a lineplot of the homicide rates of each departmen, it ist on the firt plot
    fig.add_trace(
        go.Scatter(
            x=df.state,
            y=df.rate_homicides,
            name="homicide rates",
            hoverinfo='x+y',
            line=dict(shape="spline", color="#ffab19", width=4)
        )
    )
    ##change some style of the layout
    fig.update_layout(
        legend=dict(
            yanchor="bottom",#location of the legend
            xanchor="right",#location of the legend
            bordercolor="#464646",#border color of the legend
            borderwidth=1 #border width of the legend
                    ), 
        font={'family':"Open Sans, sans-serif",'color': '#FFFFFF', 'size':13},#font of the label in the legend
        yaxis_title="",
        coloraxis_showscale=False #It hides the colorbar of the plot
    )

    return fig