import dash_core_components as dcc
import dash_html_components as html
from assets.styles import app_color
import dash_bootstrap_components as dbc
import plotly.graph_objects as go
from data.departamentos_data import DepartamentosData

departments_data = DepartamentosData()
departamentos = departments_data.getDepartamentos()

bars_explain = """Each portion of poblation hast different relevant features that impact its result, this graph shows what are the most relevant factors for the students thatwere positioned in the lowest quintile (0% to 20%), this meaning that this factors can be used to identify the portion of the students that has a higher risk of getting low scores in the Saber Pro test.
"""

radar_explain = """This radar chart shows the 5 variables that most affect the model score. The variable importance order is calculated using a game theoretic approach that takes into account interactions and correlations (http://papers.nips.cc/paper/7062-a-unified-approach-to-interpreting-model-predictions). We used the predicted score of the model to split your population into 5 groups (quintiles) going from low-score to high score. You can check the ocurrence rate (categorical variable) or the mean valeu (for numerical variable) for each of these variables, driving each group to low or high scores compared to the others. 
"""


def result(data):
    hist = go.Figure()
    hist.add_trace(
        go.Histogram(
            x=data['score'],
            histnorm='percent',
            name='Score', # name used in legend and hover labels
            marker_color='#EB89B5',
            opacity=0.75,
            nbinsx=10
        )
    )
    hist.update_layout(
        title_text='Percentual Distribution of Scores', # title of plot
        xaxis_title_text='Test Score', # xaxis label
        yaxis_title_text='Percent of students', # yaxis label
        bargap=0.2, # gap between bars of adjacent location coordinates
        bargroupgap=0.1, # gap between bars of the same location coordinates
        paper_bgcolor='rgba(0,0,0,0)',
        plot_bgcolor = 'rgba(0, 0, 0, 0)',
        font=dict(
            size=12,
            color="White"
        ),
    )
    
    table = dbc.Table.from_dataframe(data, bordered=True, dark=True, hover=True, responsive=True, striped=True)
    layout = [
        dbc.Row(
            [
                dbc.Col(
                    table,
                    width= 8
                ),
                dbc.Col(
                    dcc.Graph(id='simulation-histogram', figure=hist),
                    width=4
                )
            ]
        ),
        html.Hr(),
        dbc.Row(
            dbc.Col(
                html.H2("Understand your results:", style={'text-align': 'center', 'color': '#FFFFFF'})
            )
        ),
        html.Hr(),
        dbc.Row(
            [
                dbc.Col(
                    dcc.Dropdown(
                        id='simulation_department',
                        options=[{'label': i, 'value': i} for i in departamentos['dep_nombre'].unique()],
                        value='BOGOTA',
                        placeholder="Select a Department",
                        className='stockselector'
                    ),
                    width={"size": 5, "offset": 1}
                ),
                dbc.Col(
                    html.P("Select the department for which you want to know the most relevant features", style={'font-size': 'medium', 'color': '#FFFFFF', 'textAlign': 'left'}),
                    width={"size": 5, "offset": 1}
                )
            ]
        ),
        dbc.Row(
            [
                dbc.Col(
                    html.P(radar_explain, style={'font-size': 'medium', 'position': 'absolute', 'top': '50%', '-ms-transform': 'translateY(-50%)',   'transform': 'translateY(-50%)', 'textAlign': 'right', 'color': '#FFFFFF'}),
                    width={"size": 4, "offset": 1}
                ),
                dbc.Col(
                    dcc.Loading(
                        dcc.Graph(id='simulation_radar'),
                        type="circle",
                    ),
                    width={"size": 5, "offset": 1}
                )
            ]
        ),
        dbc.Row(
            [
                dbc.Col(
                    html.P(bars_explain, style={'font-size': 'medium', 'position': 'absolute', 'top': '50%', '-ms-transform': 'translateY(-50%)',   'transform': 'translateY(-50%)', 'textAlign': 'right', 'color': '#FFFFFF'}),
                    width={"size": 4, "offset": 1}
                ),
                dbc.Col(
                    dcc.Loading(
                        dcc.Graph(id='simulation_features_bar'),
                        type="circle",
                    ),
                    style={'align': 'left'},
                    width={"size": 5, "offset": 1}
                )
            ]
        )
    ]

    return layout
