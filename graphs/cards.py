import json
import pandas as pd
import plotly.graph_objs as go
from assets.styles import app_color
import dash_html_components as html
import dash_bootstrap_components as dbc
from data.departamentos_data import DepartamentosData

departamentosData = DepartamentosData()
errMargin = 1

def fillCard(text1, text2, logo, colorclass):
    return html.Div(
        [
            html.Div(
                [
                    html.Div(text2, style={'font-size':'small'}),
                    html.Div(text1, style={'font-size':'medium'})
                ],
                className='info'
            ),
            html.Div(
                html.I(logo, className="material-icons"),
                className='logo'+colorclass
            )
        ],
        className='card-data-icon'
    )

def getText(national, field, dep = None,):
    
    max_year = national['year'].max()
    nationalValue = round(national[national['year'] == str(max_year)][field].values[0], 2)

    if dep is None:
        return [nationalValue, 'info']


    depValue = round(dep[dep['year'] == str(max_year)][field].values[0], 2)
    
    comparator = 'info'
    if nationalValue > depValue:
        comparator = 'danger'
    if nationalValue < depValue:
        comparator = 'success'
    if field == 'homdep_tasahomicidios':
        if comparator == 'success':
            comparator = 'danger'
        elif comparator == 'danger':
            comparator = 'success'
    
    return [depValue, comparator]

def calc(selectedDep, year_slider):

    fields = ['max_punt_global', 'mean_punt_global', 'homdep_tasahomicidios']
    titles = ['Max score', 'Mean score', 'Homicides rate']
    logos =  ['trending_up','school','gavel']
    national =departamentosData.getNationalScore(years=year_slider)
    if selectedDep == None:
        deps = []
    else:
        deps = [i["text"] for i in selectedDep["points"]]
        dataDep = departamentosData.getScoreStats(departaments=deps, years=year_slider)
        dataDep = dataDep[dataDep['dep_nombre'] == deps[0]]

    max_year = max(year_slider)
    general_outline = False
    children_divs = []
    if len(deps) == 0:
        text = [getText(national, i, dep = None) for i in fields]
        cards = [ dbc.Col(fillCard(t[0], titles[i], logos[i], t[1])) for i, t in enumerate(text) ]
        cards.append(dbc.Col(fillCard('Colombia','National','place','place')))
    else:
        text = [getText(national, i, dep = dataDep) for i in fields]
        cards = [ dbc.Col(fillCard(t[0], titles[i], logos[i], t[1])) for i, t in enumerate(text) ]
        cards.append(dbc.Col(fillCard(deps[0],'Department','place','place')))

    return dbc.Row(cards)