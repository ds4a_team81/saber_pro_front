from assets.styles import app_color
import dash_html_components as html
import dash_daq as daq

#define some style for the gauges
label_style_2 = {
    "label": "Total Foreign Students",
     "style":{"color": "white"}
}

label_style_gauge_2 = {
    "label": "Foreign Avg. Score",
     "style":{"color": "white","textAlign": "center"}
}
##making two gauges, the first one is the number of students in a display
##and the second one is a odometer with the mean score
GAUG=html.Div(
                [

                    html.Div(
                    className="three columns div-for-charts bg-grey",
                    children=[daq.LEDDisplay(
                                    id='my-daq-leddisplay2',
                                    value="45778",
                                    label=label_style_2,
                                    labelPosition='top',
                                    color='white',
                                    backgroundColor="#081E43",
                                    size=30
                                    
                                    ) ,
                                daq.Gauge(
                                    id='my-daq-gauge2',
                                    min=0,
                                    label=label_style_gauge_2,
                                    labelPosition='bottom',
                                    max=200,
                                    value=75,
                                    size=180,
                                    showCurrentValue=True,
                                        color={"gradient":True,"ranges":{"red":[0,100],"yellow":[100,150],"green":[150,200]}},

                                    
                                    ) 
                            ], style={'height':20}
                        )
                ])


                         

                                            