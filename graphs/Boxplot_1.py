import pandas as pd
import plotly.graph_objs as go
from assets.styles import app_color, color_palette
import plotly.express as px
from data.departamentos_data import DepartamentosData

def createboxplot_1(df):
    
    fig = px.box(df, 
                    x="gruporeferencia", 
                    y="punt_global", 
                    labels={'punt_global':'Score','gruporeferencia':'ReferenceGroup','year':'Year'}, 
                    height=500,
                    template='plotly_dark').update_layout(
                              {
                               'plot_bgcolor': 'rgba(0, 0, 0, 0)',
                               'paper_bgcolor': 'rgba(0, 0, 0, 0)',
                               'title_text':'Reference Group vs Score'
                                }
                                )
                    
                    

    return fig