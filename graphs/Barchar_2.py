import pandas as pd
import plotly.graph_objs as go
from assets.styles import app_color, color_palette
import plotly.express as px
from data.departamentos_data import DepartamentosData

#It is a bar plot of mean score vs the nacionality of the students
def createbarchar_2(df):
    fig = px.bar(df, x='prompuntglobal', y='estu_nacionalidad',
             hover_data=['estu_nacionalidad', 'prompuntglobal', 'conteo'], 
             color='prompuntglobal',
             color_continuous_scale=color_palette,
             labels={'estu_nacionalidad':'Nacionality','prompuntglobal':'MeanScore', 'conteo':'Total students'}, 
             height=400,
             orientation='h',
             template='plotly_dark').update_layout(
                              {
                               'plot_bgcolor': 'rgba(0, 0, 0, 0)',
                               'paper_bgcolor': 'rgba(0, 0, 0, 0)',
                               'title_text':'MeanScore by Nationality',
                                'yaxis':dict(autorange="reversed")
                                },
                               coloraxis_showscale=False 
                                )
    return fig


