import pandas as pd
from assets.styles import app_color
import dash_table

table_header_style = {
    "backgroundColor": "rgb(2,21,70)",
    "color": "white",
    "textAlign": "center",
}
##obtain the table of the top universities
def createtabletop(tten):
    table_top=dash_table.DataTable(
                    id="data-table",
                    columns=[{"name": i, "id": i} for i in tten.columns],
                    data=tten.to_dict('records'),
                    
                    editable=False,
                    
                    style_header=table_header_style,
                    style_as_list_view=True,
                    style_cell={
                        'overflow': 'hidden',
                        'textOverflow': 'ellipsis',
                        'maxWidth': 0,
                        'backgroundColor': '#092355',
                        'color': 'white',
                        'textAlign': 'center'
                                },
                    style_cell_conditional=[
                        {'if': {'column_id': 'ranking'},
                        'width': '15%'},
                        {'if': {'column_id': 'promedio'},
                        'width': '15%'},
                        
                        ],
                    tooltip_data=[
                        {
                            column: {'value': str(value), 'type': 'markdown'}
                            for column, value in row.items()
                        } for row in tten.to_dict('rows')
                    ],
                    tooltip_duration=None
                )
    return table_top

            