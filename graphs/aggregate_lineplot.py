import json
import pandas as pd
import plotly.graph_objs as go
from assets.styles import app_color
import plotly.express as px
from data.departamentos_data import DepartamentosData
from assets.styles import line_colors
from assets.styles import app_color, color_palette, color_palette2


departamentosData = DepartamentosData()

def lineplot(selectedDep, year_slider):

    national =departamentosData.getNationalScore(years=year_slider)
    national.mean_punt_global = national.mean_punt_global.astype(float).round(1)



    if selectedDep == None:
        deps = []
    else:
        deps = [i["text"] for i in selectedDep["points"]]
        dataDep = departamentosData.getScoreStats(departaments=deps, years=year_slider)
    max_year = national['year'].max()
    if year_slider[0] == year_slider[1]:

        data=[
            dict(
                x='National Score', 
                y=national['mean_punt_global'].max(),
                type='line',
                name= 'National Score'
            )
        ]
        if len(deps) > 0:
            data.append(
                dict(
                    type='bar',
                    x=dataDep['dep_nombre'],
                    y=dataDep['mean_punt_global'],
                    text=dataDep['mean_punt_global'],
                    marker=dict(color=line_colors)
                )
            )
        layout = dict(
            plot_bgcolor = 'rgba(0, 0, 0, 0)',
            paper_bgcolor = 'rgba(0, 0, 0, 0)',
            title = 'Mean Score in {}'.format(max_year),
            font=dict(
                size=12,
                color="White"
            ),
            
        )
        
        return dict(data=data, layout=layout)

    data = [dict(
                type="scatter",
                mode="lines+markers",
                name=str('National Average'),
                x=national['year'],
                y=national['mean_punt_global'],
                line=dict(shape="spline", smoothing="1", color="#ffab19", width=5),
            )]

    for index, dep in enumerate(deps):
        data.append(
            dict(
                type="scatter",
                mode="lines+markers",
                name=str(dep),
                x=dataDep[dataDep['dep_nombre'] == dep]['year'],
                y=dataDep[dataDep['dep_nombre'] == dep]['mean_punt_global'],
                line=dict(shape="spline", smoothing="3", color= line_colors[index]),
            ),
        )

     

    layout = dict(
        title ="Mean score per Department",
        paper_bgcolor = 'rgba(0, 0, 0, 0)',
        plot_bgcolor = 'rgba(0, 0, 0, 0)',
        font=dict(
            size=12,
            color="White"
        ),
        xaxis=dict(
            title='Year',
            type="date",
            showgrid=True,
            color='rgba(255, 255, 255, 255)',
            width=3
        ),
        yaxis = dict(
            title='Mean Score',
            showgrid=True,
            color='rgba(255, 255, 255, 255)',
            width=5,
        )
    )

    return dict(data=data, layout=layout)