import pandas as pd
import json
import base64
import datetime
import io
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from assets.styles import app_color
import dash_bootstrap_components as dbc

ACCENTS = {
    'Ñ':'-&N-',
    'ñ':'-&n-',
    'Á':'-&A-',
    'á':'-&á-',
    'É':'-&E-',
    'é':'-&e-',
    'Í':'-&I-',
    'í':'-&i-',
    'Ó':'-&O-',
    'ó':'-&o-',
    'Ú':'-&U-',
    'ú':'-&u-',
}

def replaceAccents(content, decode=False):
    for key, value in ACCENTS.items():
        if decode:
            content = content.replace(value, key)
        else:
            content = content.replace(key, value)
    return content

def parse_contents(contents, filename, date):
    content_type, content_string = contents.split(',')

    decoded = base64.b64decode(content_string)
    try:
        if 'csv' in filename:
            # Assume that the user uploaded a CSV file
            df = pd.read_csv(io.StringIO(decoded.decode('utf-8')), sep=';')
        elif 'xls' in filename:
            # Assume that the user uploaded an excel file
            df = pd.read_excel(io.BytesIO(decoded))
    except Exception as e:
        print(e)
        return html.Div([
            'There was an error processing this file.'
        ])
    
    return [html.Div([
        dbc.Table.from_dataframe(df, bordered=True, dark=True, hover=True, responsive=True, striped=True, style={'overflowY': 'scroll'}, id='simulation_data'),
        html.Hr(),  # horizontal line
    ]), dbc.Button(html.P("Submit your data", style={'text-size':'large'}), color="success", className="mr-1", id='submit_simulation', size="xlg", block=True)], df.to_dict('index')
