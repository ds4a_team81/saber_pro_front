import dash_core_components as dcc
import dash_html_components as html
from assets.styles import app_color
import dash_bootstrap_components as dbc

SIMULATION = html.Div([
            dcc.Store(id='simulation-data', storage_type='session'),
            dbc.Row(
                [
                    dbc.Col(
                        [
                            html.H2(
                                children="Simulation",
                                style={'textAlign': 'center', 'color': '#FFFFFF'}
                                ),
                            dcc.Markdown('''
                                Add your own information about your students, this will connect with our prediction engines and provide you the information needed to take the best decisions in your entity
                                
                                1. Download [this template](https://ds4a-public.s3.amazonaws.com/saberpro_mock_template.csv).  
                                2. Fill it template with your students infomation.  
                                3. Drag and Drop your file in the upload module or click on the module and select the file.  
                                4. Click the 'Submit' button.
                                ''',
                                style={'font-size':'medium'}),    
                            html.Div(id='simu-button-space')
                        ],
                        width=4
                    ),
                    dbc.Col(
                        [
                            dcc.Upload(
                                id='upload-data',
                                children=html.Div([
                                    'Drag and Drop or ',
                                    html.A('Select Files')
                                ]),
                                style={
                                    'width': '100%',
                                    'height': '60px',
                                    'color': '#FFFFFF',
                                    'lineHeight': '60px',
                                    'borderWidth': '2px',
                                    'borderStyle': 'dashed',
                                    'borderRadius': '5px',
                                    'textAlign': 'center',
                                    'margin': '10px'
                                },
                                # Allow multiple files to be uploaded
                                multiple=True
                            ),
                            html.Div(id='output-data-upload'),
                        ],
                        width=8
                    )
                ]
            )
        ],
        id='simulation_result')
