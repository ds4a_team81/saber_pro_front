import dash_core_components as dcc
import dash_html_components as html
from assets.styles import app_color
import pandas as pd
import dash_bootstrap_components as dbc

EXPLORATORY = dbc.Row(
    dbc.Col(
    children=[
        html.Div(
            children=[html.Div(
                    className="five columns",##space for the first column that contains the year selector and the map
                    children=[
                        dcc.Loading(
                            id="loading-1",
                            children=[
                                ##a little space for better style
                                html.Div(
                                    children=" ",
                                    style={'height':15}    
                                        ),
                                ##Title for year selector
                                html.P(
                                    children="Year selector",
                                    style={'textAlign': 'center', 'color': '#FFFFFF', 'font-size':'medium', 'height':60}
                                ),
                                ##A dash core component for a year selector
                                dcc.RangeSlider(
                                    id="year_slider",
                                    min=2016,
                                    max=2019,
                                    value=[2016, 2019],
                                    marks={
                                        2016: {'label': '2016', 'style': {'color': '#FFFFFF'}},
                                        2017: {'label': '2017', 'style': {'color': '#FFFFFF'}},
                                        2018: {'label': '2018', 'style': {'color': '#FFFFFF'}},
                                        2019: {'label': '2019', 'style': {'color': '#FFFFFF'}},
                                    }
                                ),
                                ##A dash core component for the map
                                dcc.Graph(
                                    id="choropleth",
                                    config={
                                        'displayModeBar': True,
                                        'modeBarButtonsToRemove': ['lasso2d','pan2d', 'zoomInGeo',##hide some tools that dont give value to the map
                                         'zoomOutGeo', 'hoverClosestGeo', 'toImage','select2d'],
                                    }
                                ),
                                ##A Dash Bootstrap Component for add some
                                ##instruntions when the user put the mouse on the map 
                                dbc.Tooltip(
                                    "Select a department. You can select more than one department keeping pressed SHIFT and right clicking.",
                                    target="choropleth",
                                    placement='right',
                                    style={'font-size':'medium'}, 
                                ),
                            ],
                            type="circle",##the tipe of the loading bar
                        ),
                        
                    ],
                ),
                html.Div(
                    [
                        ##the space for the cards
                        dbc.Row(
                                dbc.Col( 
                                    id="info-container",
                                    children=[],
                                )
                        ),
                        ##the space for the agegate graph
                        dbc.Row(
                            dbc.Col(
                                dcc.Graph(
                                    id="aggregate_graph",
                                    config={
                                        'displayModeBar': False
                                    }
                                )
                            )
                        ),
                        ##the space for the table
                         dbc.Row(
                            dbc.Col(
                                children=[
                                     html.P(
                                     children="Top 5 Universities",
                                     style={'textAlign': 'center', 'color': '#FFFFFF', 'font-size':'medium'}    
                                        ), 
                                    html.Div(
                                        id='tabletop',
                                        children=[],style={'textAlign': 'center', 'color': '#0d00ff','backgroundColor': 'rgb(248, 248, 248)'}  
                                        

                                    )
                                
                                ]

                               
                            )
                        )
            ],
                    className="seven columns",##space for the second column that contains the cards, the agregate graph and the table
                )

            ],
        ),
    ],
)
)