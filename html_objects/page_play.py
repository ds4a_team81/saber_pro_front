import dash_core_components as dcc
import dash_html_components as html
from assets.styles import app_color
from data.db_query import DAO
import pandas as pd
import dash_daq as daq
from data.departamentos_data import DepartamentosData
from data.db_query import DAO
from utils import make_dash_table

from graphs.Gauges import GAUG

import dash_bootstrap_components as dbc


departments_data = DepartamentosData()
departamentos = departments_data.getDepartamentos()
countrieslist = departments_data.getcountries()

label_style_2 = {
    "label": "Total Students",
    "style": {"color": "white"}
}


label_style_gauge_2 = {
    "label": "Mean Score",
    "style": {"color": "white", "textAlign": "center"}
}


PLAY = dbc.Row(
    dbc.Col(
        children=[
            html.Div(
                children=[
                    html.Div(
                        className="four columns",
                        children=[
                            html.H1('Play with the variable analysis',
                                    style={'textAlign': 'center',
                                           'color': '#FFFFFF', 'font-size': 'medium'}
                                    ),
                            html.Div(
                                children=" ",
                                style={'height': 20}
                            ),

                            html.H2('''Select any value to see more information.''',
                                    style={'textAlign': 'center',
                                           'color': '#FFFFFF', 'font-size': 'medium'}
                                    ),

                            html.Div(
                                children=" ",
                                style={'height': 20}
                            ),

                            html.P(children="Year Slider",
                                   ),

                            html.Div(
                                children=" ",
                                style={'height': 20}
                            ),

                            dcc.RangeSlider(
                                id="year_slider_play",
                                min=2016,
                                max=2019,
                                value=[2016, 2019],
                                marks={
                                    2016: {'label': '2016', 'style': {'color': '#FFFFFF'}},
                                    2017: {'label': '2017', 'style': {'color': '#FFFFFF'}},
                                    2018: {'label': '2018', 'style': {'color': '#FFFFFF'}},
                                    2019: {'label': '2019', 'style': {'color': '#FFFFFF'}},
                                }
                            ),

                            html.Div(
                                children=" ",
                                style={'height': 20}
                            ),
                            html.P(
                                children="State selector",
                                style={'textAlign': 'center',
                                       'color': '#FFFFFF', 'font-size': 'medium'}
                            ),

                            dcc.Dropdown(id='stateselector',
                                         options=[{'label': i, 'value': i}
                                                  for i in departamentos['dep_nombre'].unique()],
                                         multi=True,
                                         value=departamentos['dep_nombre'].sort_values(
                                         ),
                                         style={'backgroundColor': '#1E1E1E'},
                                         className='stockselector'),
                            html.Div(
                                children=" ",
                                style={'height': 40}
                            ),


                            html.P(
                                children="Country Selector",
                                style={'textAlign': 'center',
                                       'color': '#FFFFFF', 'font-size': 'medium'}
                            ),

                            dcc.Dropdown(id='countryselector',
                                         options=[
                                             {'label': i, 'value': i} for i in countrieslist['estu_nacionalidad'].unique()],
                                         multi=True,
                                         value=countrieslist.loc[countrieslist['estu_nacionalidad'].isin(
                                             ['ARGENTINA', 'COLOMBIA', 'CHILE', 'MEXICO', 'PERU'])]['estu_nacionalidad'].sort_values(),
                                         style={'backgroundColor': '#1E1E1E'},
                                         className='stockselector')


                        ],
                    )

                ],
            ),
            html.Div(
                children=[html.Div(
                    className="eight columns",
                    style={'overflow-y': 'auto'},
                    children=[
                        dcc.Loading(
                            children=[
                                html.Div(
                                    id='my-barchar1',
                                    className="twelve columns div-for-charts bg-grey",
                                    style={
                                        'margin': 1, 'padding': 5},
                                    children=[dcc.Graph(id="barchar_1")],

                                )],
                            type="circle",
                        ),
                        dcc.Loading(
                            children=[
                                html.Div(
                                    id='my-barchar2',
                                    className="eight columns div-for-charts bg-grey",
                                    style={
                                        'margin': 1, 'padding': 5},
                                    children=[dcc.Graph(id="barchar_2")]
                                )],
                            type="circle",
                        ),
                        dcc.Loading(
                            children=[
                                html.Div(
                                    className="three columns div-for-charts bg-grey",

                                    children=[

                                        html.Div(
                                            children=" ",
                                            style={'height': 20}
                                        ),

                                        daq.LEDDisplay(
                                            id='my-daq-leddisplay2',
                                            value="0000",
                                            label=label_style_2,
                                            labelPosition='top',
                                            color='white',
                                            backgroundColor="#081E43",
                                            size=30

                                        ),
                                        daq.Gauge(
                                            id='my-daq-gauge2',
                                            min=0,
                                            labelPosition='bottom',
                                            label=label_style_gauge_2,
                                            max=200,
                                            value=0,
                                            size=180,
                                            showCurrentValue=True,
                                            color={"gradient": True, "ranges": {
                                                "red": [0, 100], "yellow":[100, 150], "green":[150, 200]}},


                                        )
                                    ]
                                )],
                            type="circle",
                        ),
                        dcc.Loading(
                            children=[
                                html.Div(
                                    id='my-scatter_1',
                                    className="eleven columns div-for-charts bg-grey",
                                    style={
                                        'margin': 1, 'padding': 5},
                                    children=[dcc.Graph(id="scatter_1")]
                                )],
                            type="circle",
                        ),
                        dcc.Loading(
                            children=[
                                html.Div(
                                    id='my-boxplot_1',
                                    className="eleven columns div-for-charts bg-grey",
                                    style={
                                        'margin': 1, 'padding': 5},
                                    children=[dcc.Graph(id="boxplot_1")]
                                )],
                            type="circle",
                        ),

                    ],
                )

                ],
            ),
        ]
    )
)
