import dash_core_components as dcc
import dash_html_components as html
from assets.styles import app_color
import dash_bootstrap_components as dbc

#define the style parameter for all the cards
W_p = "28rem"
style_des={'font-size':'14px', "textAlign": "center"}

#each card load a picture and define a specific text
alejandro = dbc.Card(
    [
        dbc.CardImg(
            src="https://ds4a-public.s3.amazonaws.com/team_pictures/cdba9af0-3f88-420f-995b-bdee1ee23e28.jpeg"),##load the picture on amazon aws
        dbc.CardBody(
                        dcc.Markdown('''**Alejandro Zabala Camacho**, Electrical engineer and cycling lover.
                                        Interested in the application of machine learning for social 
                                        purposes and for the development of renewable energies.
                                    ''', style=style_des,  className="card-text"),##Descripction of the person
        ),
    ],
    style={"width": W_p},
)
elias = dbc.Card(
    [
        dbc.CardImg(
            src="https://ds4a-public.s3.amazonaws.com/team_pictures/3ad5f3c0-35dc-43bb-9215-5c8f9aa1d294.jpeg"),
        dbc.CardBody(
                        dcc.Markdown('''**Campo Elías Abril Leal**, Statistician / Actuary who loves sports and
                                         travel. Interested in analytical models that transform the industry.  
                                         
                                         ''',
                                         style=style_des, className="card-text"),
        ),
    ],
    style={"width": W_p},
)
fabio = dbc.Card(
    [
        dbc.CardImg(
            src="https://ds4a-public.s3.amazonaws.com/team_pictures/e56fd324-65f6-45dd-ae23-da1b26810cd5.jpeg"),
        dbc.CardBody(
                        dcc.Markdown('''**Fabio Andrés Parra Fuentes**, Systems Engineer, passionate for 
                        physics and food. I have big interest in apply math models and processes to social 
                        contexts in order to obtain great insights.''', style=style_des),
        ),
    ],
    style={"width": W_p},
)
oscar = dbc.Card(
    [
        dbc.CardImg(
            src="https://ds4a-public.s3.amazonaws.com/team_pictures/c7052f8e-f176-4ee1-911e-795b0f1d5de4.jpeg"),
        dbc.CardBody(
                        dcc.Markdown('''**Oscar Javier Ramirez Amaya**, Telematics Engineer. PMP and CSM. 
                        A cycling and football fan, with a huge love for the music. 
                        Interested in software development and  IT project management related to 
                        transportation and route optimization.''',
                         style=style_des),
        ),
    ],
    style={"width": W_p},
)
roberto = dbc.Card(
    [
        dbc.CardImg(
            src="https://ds4a-public.s3.amazonaws.com/team_pictures/fd5e4e42-89f6-4fd8-91b7-4234277fc618.jpeg"),
        dbc.CardBody(
                        dcc.Markdown('''**Roberto Vargas Cruz**, Systems Engineer and runner. My main interest
                        is to add value to stakeholders through technology and business knowledge.''',
                         style=style_des),
        ),
    ],
    style={"width": W_p},
)
william = dbc.Card(
    [
        dbc.CardImg(
            src="https://ds4a-public.s3.amazonaws.com/team_pictures/e899ac28-75e4-4467-86a5-bc03180a8933.jpeg"),
        dbc.CardBody(
                        dcc.Markdown('''**William Javier Garcia Herrera**, Electronic engineer with PhD in
                         electrical and computing engineering. With expertise in programming and strong 
                         interest for data science and machine learning applied to research and industry.''',
                         style=style_des),
        ),
    ],
    style={"width": W_p},
)
#Define the layaout for the cards, three columns, the firts row for the title and the others two for the cards
ABOUT = html.Div(
    [   dbc.Row(
            [
                dbc.Col(
                [
                    html.H1('Developers'),
                ]
            )  
            ]            
        ),    
        dbc.Row(
            [
                dbc.Col(alejandro),
                dbc.Col(elias),
                dbc.Col(fabio,),
            ]
        ),
        dbc.Row(
            [
                dbc.Col(oscar),
                dbc.Col(roberto),
                dbc.Col(william),
            ]
        )
    ],
)