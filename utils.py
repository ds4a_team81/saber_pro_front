"""
Function that provides some core functions that can be used for any component
"""

import boto3
import dash_html_components as html
import dash_core_components as dcc
import uuid
from botocore.exceptions import ClientError

def make_dash_table(df):
    """
    Return a dash definition of an HTML table for a Pandas dataframe
    """
    table = []
    for index, row in df.iterrows():
        html_row = []
        for i in range(len(row)):
            html_row.append(html.Td([row[i]]))
        table.append(html.Tr(html_row))
    return table

def upload_file(filename):
    """
    Upload a file to the public-datasets bucket provided to make 
    the real time analysis
    """
    object_name = 'public-datasets/'+str(uuid.uuid4())+'.csv'
    s3_client = boto3.client('s3')
    try:
        response = s3_client.upload_file(filename, 'ds4a-code', object_name)
    except ClientError as e:
        print(e)
        return False, object_name
    return True, object_name