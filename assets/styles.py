app_color = {
    "graph_bg": "rgba(0,0,0,0)", #082255
    "graph_line": "#007ACE", 
    "page_bg": '#122b54'##122b54
    }
line_colors = ['#7221ff', '#d321ff', '#ff216f', '#21d6ff', '#26ff26', '#d8576b', '#ed7953', '#fb9f3a', '#fdca26', '#f0f921']
color_palette = 'Cividis_r' #YlGnBu, speed, Aggrnyl_r, Cividis_r, Geyser, Tealrose, Tropic
color_palette2 = 'Tealrose' #YlGnBu, speed, Aggrnyl_r, Cividis_r, Geyser, Tealrose, Tropic